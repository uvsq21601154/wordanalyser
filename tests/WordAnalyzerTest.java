import org.junit.Test;

import static org.junit.Assert.*;

public class WordAnalyzerTest {

    @Test
    public void firstRepeatedCharacter() {
        assertEquals(new WordAnalyzer("aardvark").firstRepeatedCharacter(),'a');
        assertNotEquals(new WordAnalyzer("roommate").firstMultipleCharacter(),'m');
        assertEquals(new WordAnalyzer("roommate").firstRepeatedCharacter(),'o');
        assertEquals(new WordAnalyzer("mate").firstRepeatedCharacter(),0);
        assertNotEquals(new WordAnalyzer("test").firstRepeatedCharacter(),'t');
        assertEquals(new WordAnalyzer("test").firstRepeatedCharacter(),0);
    }

    @Test
    public void firstMultipleCharacter() {
        assertNotEquals(new WordAnalyzer("missisippi").firstMultipleCharacter(),'m');
        assertNotEquals(new WordAnalyzer("missisippi").firstMultipleCharacter(),'s');
        assertEquals(new WordAnalyzer("missisippi").firstMultipleCharacter(),'i');
        assertEquals(new WordAnalyzer("mate").firstMultipleCharacter(),0);
        assertEquals(new WordAnalyzer("test").firstMultipleCharacter(),'t');
    }

    @Test
    public void countRepeatedCharacters() {
        assertEquals(new WordAnalyzer("mississippiii").countRepeatedCharacters(),4);
        assertEquals(new WordAnalyzer("test").countRepeatedCharacters(),0);
        assertEquals(new WordAnalyzer("aabbcdaaaabb").countRepeatedCharacters(),4);
    }
}